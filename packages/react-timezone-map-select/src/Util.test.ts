import { convertOffsetInMinutesToString, findTimeZone } from './Util';

describe('convertOffsetInMinutesToString', () => {
    test('plus offset', () => {
        const strVal = convertOffsetInMinutesToString(60);
        expect(strVal).toBe('+01:00');
    })
    test('minus offset', () => {
        const strVal = convertOffsetInMinutesToString(-120);
        expect(strVal).toBe('-02:00');
    })
});

describe('findTimeZone', () => {
    test('test a normal case.', () => {
        const timezone = findTimeZone('America/Los_Angeles');
        expect(timezone?.name).toBe('America/Los_Angeles');
        expect(timezone?.countryName).toBe('United States');
    });
    test('test an error case.', () => {
        const timezone = findTimeZone('invalid name');
        expect(timezone).toBeUndefined();
    });
});
